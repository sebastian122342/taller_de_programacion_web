/* Zona calculadora */

// Sacar todas las teclas
const calc_keys = document.getElementById('calc-keys');
const real_keys = calc_keys.children;

// Sacar la pantalla de la calculadora
var display = document.getElementById('display');

// Secuencialmente, ir agregando eventos a todos los botones
for (let i = 0; i < real_keys.length; i++) {

    const child_btn = real_keys[i];

    if (!child_btn.classList.contains("all-clear") && !child_btn.classList.contains("equal-sign")) {
        child_btn.addEventListener("click", simple_input);

    } else if (child_btn.classList.contains("all-clear")) {
        child_btn.addEventListener("click", clear_calc);

    } else if (child_btn.classList.contains("equal-sign")) {
        child_btn.addEventListener("click", compute);
    }
};

/* Zona adivinanza */

// Etiquetas que se usan
const btn_guess = document.getElementById("guess-button");
const btn_reset = document.getElementById("reset-guess");
const entry_guess = document.getElementById("guess-input");
const health_count = document.getElementById("health-count");

// Contador de vidas restantes
var health = 3;
health_count.value = health;

// Se determina aleatoriamente un numero ganador
var winner = Math.floor(Math.random() * 21);
console.log("El numero ganador es ", winner);

// Añadir eventos a las etiquetas
btn_guess.addEventListener("click", guess);
btn_reset.addEventListener("click", reset_game);
entry_guess.addEventListener("input", limit_inputlen);

/* Zona de Lotería */

// Etiquetas que se usan
const lotery_button = document.getElementById("lotery-button");
const lotery_reset = document.getElementById('lotery-reset');
const lotery_result = document.getElementById("lotery-result");

const lotery_nums = document.querySelectorAll('.lotnum');
const lotnum_array = Array.from(lotery_nums);

// Determinar un ganador
var lot_winner = make_winner();
console.log("Ganador de la lotería: ", lot_winner)

// Añadir eventos
lotery_button.addEventListener("click", check_lotery);
lotery_reset.addEventListener("click", reset_lotery);

lotnum_array.forEach(entry => {
    entry.addEventListener("input", limit_inputlen);
});

/* Zona lista de tareas */
const task_table = document.getElementById("task-table");
const add_task_button = document.getElementById("add-task");
const del_task_button = document.getElementById("del-task");
const entry_task = document.getElementById("task-entry");
const info_task_btn = document.getElementById("info-task");

// Añadir eventos
add_task_button.addEventListener("click", add_task);
del_task_button.addEventListener("click", del_task);
entry_task.addEventListener("input", check_existence);
entry_task.addEventListener("keypress", add_with_enter);
task_table.addEventListener("click", mark_row);
info_task_btn.addEventListener("click", info_task);

/* Zona del calculo del digito verificador */

// Etiquetas usadas
const run_input = document.getElementById("entry-run");
const dig_verif_input = document.getElementById("dig-verif");

// Añadir los eventos
run_input.addEventListener("input", limit_inputlen);
run_input.addEventListener("input", check_dig_verif);

/* Todas las funciones 
Hay algunas funciones que son compartidas entre más de un ejercicio, como la de limitar
la cantidad de caracteres en una entrada numérica o la de verificar si un numero está dentro
de unos rangos dados.
*/

// Limitar la cantidad de caracteres en una entrada numérica
function limit_inputlen() {
    // Solamente se aceptarán numeros enteros
    event.target.value = event.target.value.replace(".", "");
    event.target.value = event.target.value.replace("+", "");
    event.target.value = event.target.value.replace("-", "");
    // Dependiendo de la entrada, se limitará el largo máximo de caracteres de manera diferenciada
    if (event.target.id === "entry-run") {
        event.target.value = event.target.value.slice(0, 8);
    } else {
        event.target.value = event.target.value.slice(0, 2);
    }
}

// Añadir una tarea a la lista de tareas
function add_task() {
    // Numero de la nueva fila
    const newrow = task_table.rows.length + 1;
    // Agregar una nueva fila
    const row = task_table.insertRow();
    // Determinar un id para la nueva fila
    row.id = "task" + newrow.toString();

    // Primera celda para checkbox, segunda para el texto en si
    const cell1 = row.insertCell();
    const cell2 = row.insertCell();

    // Poner la checkbox en la celda
    cell1.innerHTML = cell1.innerHTML + '<input type="checkbox" id="check' + newrow.toString() + '">';

    // Poner el texto en la segunda celda
    cell2.id = "task" + newrow.toString() + "-name";
    cell2.innerHTML = cell2.innerHTML + "<div contenteditable>" + entry_task.value + "</div>";

    // Ahora que se ha agregado la tarea, resetear la entrada y el boton
    entry_task.value = "";
    add_task_button.disabled = true;
}

// Se pueden agregar tareas al presionar enter
function add_with_enter() {
    // Si se presiona enter, se llama a la misma funcion de agregar tarea normal
    if (event.code === "Enter") {
        // Mantener el cuidado con deshabilitar o habilitar el boton de agregar
        if (add_task_button.disabled === false) { add_task(); }
    }
}

// Marcar una fila clickeada en la tabla de tareas
function mark_row() {
    var parent = event.target.parentNode;
    // Solamente se acepta que se clickee en la fila en sí. clickear la checkbox o la tarea para editarla no marca la fila
    if (parent.tagName === "TD") {
        const child = parent.children[0];
        // Si se clickea la checkbox entonces se tacha/destacha el texto de la tarea
        if (child.tagName === "INPUT") {
            // Tachar el otro elemento de la fila
            if (child.checked) {
                parent.parentNode.children[1].children[0].style.textDecoration = "line-through";
            } else {
                parent.parentNode.children[1].children[0].style.textDecoration = "";
            }
        }
        return
    }
    // Toggle para cambiar el color de fondo de la row
    if (parent.classList.contains("table-danger")) {
        parent.classList.remove("table-danger");
    } else {
        parent.classList.add("table-danger");
    }
}

// Borrar 1 o mas tareas. Se borran todas las tareas marcadas
function del_task() {
    // Tomar todas las rows que esten marcadas y eliminarlas
    const rows = Array.prototype.slice.call(task_table.rows);
    console.log(rows);
    rows.forEach(row => {
        if (row.classList.contains("table-danger")) {
            task_table.deleteRow(row.rowIndex - 1);
        }
    });
}

function info_task() {
    alert("Click en la fila para marcarla, despues click en eliminar para borrarla. Click en la tarea para editarla.");
}

// Condicionar la sensibilidad del boton de agregar dependiendo de si hay texto en la entrada de tarea o no
function check_existence() {
    if (event.target.value != "") {
        add_task_button.disabled = false;
    }
    else {
        add_task_button.disabled = true;
    }
}

// Calcular el digito verificador del RUN. Se aplica cada vez que se ingresan digitos a la entrada de RUN
// Se limita el tamaño del RUN a 8 digitos, sin limite inferior.
function check_dig_verif() {
    // Aplicar el algoritmo según se investigó
    var run_backwards = run_input.value.split('').reverse().join('');
    var sum = 0;
    var start = 2;
    for (let i = 0; i < run_input.value.length; i++) {
        sum = sum + start * parseInt(run_backwards[i]);
        start = start + 1;
        if (start > 7) { start = 2 };
    }
    var verif = 11 - sum % 11;
    if (verif === 11) { verif = 0 }
    if (verif === 10) { verif = "K" }
    dig_verif_input.value = verif;
}

// Sacar el intento de lotería
function get_lot_try() {
    // Se obtienen los numeros de todas las entradas y se colocan en un arreglo
    const lotnums = [];
    lotnum_array.forEach(lotnum => {
        lotnums.push(lotnum.value);
    });
    return lotnums;
}

// Jugar a la lotería en sí
function check_lotery() {
    const lotnums = get_lot_try(); // El intento
    console.log(lotnums);
    const passes = check_lot_valid(lotnums); // Verificar si es válido el intento
    console.log(passes);

    // Todos los numeros deben ser validos, ni uno equivocado
    if (passes.includes(false)) {
        alert("Entrada no válida! Intenta de nuevo");
        return;
    }

    // Revisar que cada numero i de los intentos sea igual al numero i de los ganadores
    const revs = [];
    for (let i = 0; i < 6; i++) {
        if (lotnums[i] === lot_winner[i]) {
            revs.push(true);
        } else {
            revs.push(false);
        }
    }

    // Todo tiene que estar bien para ganar
    if (revs.includes(false)) {
        alert("Perdiste!");
    } else {
        alert("Ganaste!");
    }

    // Quitar la sensibilidad de las entradas, pintar de verde las buenas y de rojo las malas
    for (let i = 0; i < 6; i++) {
        console.log(lotnum_array[i]);
        lotnum_array[i].disabled = true;
        if (revs[i]) {
            // Cambiar a verde
            lotnum_array[i].style.color = "green";
        } else {
            // Poner en rojo
            lotnum_array[i].style.color = "red";
        }
    }

    // Mostrar el resultado real en la entry. Estaba invisible y ahora es visible
    lotery_result.value = lot_winner.join("-");
    lotery_result.style.color = "green";
    lotery_result.classList.remove("res-lot");

}

// Verificar que el intento de lotería sea válido
function check_lot_valid(lotnums) {
    // Solamente numeros enteros, los rangos se verifican en una funcion aparte
    var passes = [];
    lotnums.forEach(lotnum => {
        const int_num = parseInt(lotnum);
        if (isNaN(int_num)) {
            passes.push(false);
        } else {
            passes.push(check_lims(1, 36, int_num)); // Se revisa el rango
        }
    });
    return passes;
}

// Resetear el juego de lotería, para jugar de nuevo sin recargar la pagina completa
function reset_lotery() {
    // Cambiar el color a negro de nuevo, devolver sensibilidad, volver a rollear el winner, eliminar lo ya ingresado, volver a ocultar la entry de resultado
    lot_winner = make_winner();
    lotnum_array.forEach(entry => {
        entry.value = "";
        entry.style.color = "black"
        entry.disabled = false;
    });
    lotery_result.classList.add("res-lot");
    console.log("Ganador de la lotería: ", lot_winner)
}

// Generar una serie de numeros ganadores de la lotería
function make_winner() {
    // Generar seis numeros aleatorios, del 1 al 36.
    var temp = [];
    for (let i = 0; i < 6; i++) {
        temp.push((Math.floor(Math.random() * 36) + 1).toString());
    };
    return temp;
}

// Resetear rl juego de adivinanza
function reset_game() {
    // Reiniciar las vidas, la entrada, el ganador y las sensibilidades
    alert("Reiniciando!");
    health = 3;
    health_count.value = health;
    entry_guess.value = "";
    winner = Math.floor(Math.random() * 20) + 1;
    console.log("El ganador es ", winner);
    btn_guess.disabled = false;
    entry_guess.disabled = false;
}

// Revisar si se puede seguir jugando o no
function check_status() {
    if (health === 0) {
        // Si se pierde no se puede hacer nada más que reiniciar el juego
        alert("perdiste!");
        btn_guess.disabled = true;
        entry_guess.disabled = true;
        entry_guess.value = "";
    }
}

// Revisar que un numero val esté entre los valores inf y sup. Se usa en más de un juego
function check_lims(inf, sup, val) {
    if (val > sup || val < inf) { return false }
    return true;
}

// Jugar a la adivinanza en sí
function guess() {
    const try_int = parseInt(entry_guess.value); // El intento

    // No se descuentan vidas por intentos de numeros invalidos.

    // Por si se quiere jugar con una entrada vacía, principalmente
    if (isNaN(try_int)) {
        alert("¡No se ha ingresado un numero válido!");
        entry_guess.value = "";
        return;
    }

    // Verificar que esté entre rangos
    var pass = check_lims(0, 20, try_int);
    if (!pass) {
        alert("El numero ingresado no está en los rangos adecuados!");
        entry_guess.value = "";
        return;
    }

    // Si el numero es valido, ver si ganó
    if (try_int == winner) {
        alert("Ganaste!");
        reset_game();
    } else if (try_int > winner) {
        alert("Muy grande! Disminuye el valor");
        health = health - 1;
    } else {
        alert("Muy pequeño! Aumenta el valor");
        health = health - 1;
    }

    // Actualizar la vida y chequear el status
    health_count.value = health;
    check_status();
}


// Para todos los inputs de la calculadora que no sean limpiar y el signo igual
function simple_input() {
    // No se puede operar con valores inválidos
    if (display.value === "Infinity" || display.value === "NaN") { display.value = "" };
    // Agregar el input a la pantalla
    display.value = display.value + event.target.value;
}

// Para limpiar la pantalla
function clear_calc() {
    display.value = "";
}

// Pre-procesar la expresior para su correcto manejo
function parse_expression(expression) {
    var arr_exp = expression.split("");
    var new_exp = [];
    for (let i = 0; i < expression.length; i++) {
        if (arr_exp[i] === "-") {
            if (i != 0) {
                if (arr_exp[i - 1] != "*" && arr_exp[i - 1] != "/") {
                    // añadir un + en la posicion i-1
                    new_exp.push("+");
                    new_exp.push("-");
                } else {
                    new_exp.push("-");
                }
            } else {
                new_exp.push(arr_exp[i]);
            }
        } else {
            new_exp.push(arr_exp[i]);
        }
    }
    return new_exp.join("");
}

/* Computar el cálculo de la calculadoraw

Como tal, la calculadora realiza tres operaciones:
Suma, multiplicacion, division.
La resta se interpreta como suma de uno o más numeros negativos.
El porcentaje, se interpreta como una multiplicación por 0.01 de otra multiplicación.
Por ejemplo, 100%10 es el 100% de 10 que es 10, que es igual al 10%100 que es el 10% de 100 que es 10.
Por lo que es válido decir que x%y = x*0.01*y.

en una expresion como 4*5+7, se separan en dos arreglos operadores y numeros.
Primero multiplicaciones y divisiones de izquierda a derecha, luego sumas y restas tambien en ese orden.

*/

function compute() {
    var expression = display.value;
    if (expression === "") { return } // No se calcula si no hay nada
    // Cambiar los % por multiplicaciones.
    expression = expression.replace(/%/g, "*0.01*");

    // Hacer el procesamiento como cuidado de los signos -, ya que no se interpretan como un operador
    expression = parse_expression(expression);

    // Como referencia, se tiene un "calculo real" sacado con eval
    console.log("Calculo real: ", eval(expression));

    // Separar la cadena de texto en numeros y operadores.
    var numbers = expression.split(/[+*/]/);
    var operators = expression.match(/[+*/]/g);

    // Primero depletar los * y /, pues tienen prioridad.
    const first_depl = deplete_op(numbers, operators, "*", "/");

    // Actualizar numeros y operadores, ahora que ya se han hecho multiplicaciones y divisiones
    numbers = first_depl[0];
    operators = first_depl[1];

    // Como tal, no es necesario pasar el "-", ya que no se condiera como operador
    const second_depl = deplete_op(numbers, operators, "+", "");
    numbers = second_depl[0];
    // para este punto operators deberia estar vacio, asi que no se recupera eso
    console.log("Resultado calculado", numbers[0]);

    //Mostrar el resultado
    display.value = numbers[0];
}

// operar hasta que se acabe cierto operador
// se termina devolviendo el nuevo arreglo de operadores y numeros
// se reciben los arreglos de operadores, numeros y los operadores a usar.
function deplete_op(num_arr, ops_arr, op1, op2) {
    // Se trabaja hasta que se acaben los operadores objetivo
    while (ops_arr.includes(op1) || ops_arr.includes(op2)) {
        // Sacar el menor index, pero que ese index no sea < 0, ya que debe existir para ser usado
        // Si el index da -1 entonces no existe en el arreglo, asi que no existe.
        // Se saca el menor indice que no sea < 0 ya que se van haciendo operaciones "de izquierda a derecha"

        var indexes = [];
        if (ops_arr.indexOf(op1) >= 0) { indexes.push(ops_arr.indexOf(op1)); }
        if (ops_arr.indexOf(op2) >= 0) { indexes.push(ops_arr.indexOf(op2)); }
        let index = Math.min.apply(null, indexes);

        // si el operador objetivo esta en el indice 1 entonces los numeros a usar seran los 1 y 2
        // como en el while se condiciona a que exista el operador entonces no es necesario verificar que index sea -1
        // se sabe que al menos uno de los dos operadores se encontrará en el arreglo

        // Sacar lo que se va a usar, eliminarlo de su arreglo original
        let op = ops_arr[index];
        ops_arr.splice(index, 1);

        let num1 = to_number(num_arr[index]);
        let num2 = to_number(num_arr[index + 1]);

        // Devolver el resultado al arreglo de numeros, en la posicion que le corresponda
        let res = operate(num1, num2, op);
        num_arr[index] = res.toString();
        num_arr.splice(index + 1, 1);
    }
    // Devolver numeros y operadores
    return [num_arr, ops_arr];
}

// Hacer la operacion, dependiendo del operador
function operate(num1, num2, operator) {
    if (operator === "*") {
        return num1 * num2;
    } else if (operator === "/") {
        return (num1 / num2).toFixed(3); // Divisiones con 3 decimales para no llenar la pantalla
    } else if (operator === "+") {
        return num1 + num2;
    }
}

// Pasar el numero imputado como texto a numero
function to_number(num_str) {
    return parseFloat(num_str);
}